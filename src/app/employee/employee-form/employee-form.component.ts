import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})

export class EmployeeFormComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {
  }

  isSubmited: boolean = false

  employeeForm = new FormGroup(
    {
      sn: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
      name: new FormControl('', [Validators.required]),
      code: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
      joinedDate: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      gender: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      position: new FormControl('', [Validators.required]),
      mobileNumber: new FormControl('', [Validators.required]),
    }
  )


  onSubmit() {
    this.isSubmited = true;
    if (this.employeeForm.valid) {
      console.log(this.employeeForm.value)
    } else {
      // validate all form fields
      this.employeeForm.validator
    }
  }
}
