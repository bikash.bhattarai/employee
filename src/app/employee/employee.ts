export interface IEmployee {
  sn: number;
  name: string;
  code: number;
  joinedDate: string;
  email: string;
  gender: string;
  address: string;
  position: string;
  mobileNumber: string;
}


export class Employee {

  constructor(
    public sn: number,
    public name: string,
    public code: number,
    public joinedDate: string,
    public email: string,
    public gender: string,
    public address: string,
    public position: string,
    public mobileNumber: string
  ) { }

}