import { Component, OnInit } from '@angular/core';
import { IEmployee } from '../employee';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})


export class EmployeeListComponent implements OnInit {

  employees: IEmployee[] = [
    {
      "sn": 0,
      "name": "Kayla Holcomb",
      "code": 386,
      "joinedDate": "Sat Jul 23 2005 19:39:43 GMT+0545 (Nepal Time)",
      "gender": "female",
      "email": "kaylaholcomb@vitricomp.com",
      "position": "Engineer",
      "mobileNumber": "+977 (832) 596-3640",
      "address": "907 Rost Place, Leroy, Missouri, 2952"
    },
    {
      "sn": 1,
      "name": "Tasha Foreman",
      "code": 430,
      "joinedDate": "Tue Jan 21 2020 13:04:00 GMT+0545 (Nepal Time)",
      "gender": "female",
      "email": "tashaforeman@vitricomp.com",
      "position": "Intern",
      "mobileNumber": "+977 (982) 550-3178",
      "address": "368 Liberty Avenue, Macdona, Delaware, 2860"
    },
    {
      "sn": 2,
      "name": "Twila Cummings",
      "code": 499,
      "joinedDate": "Thu Jun 30 1983 20:58:20 GMT+0530 (Nepal Time)",
      "gender": "female",
      "email": "twilacummings@vitricomp.com",
      "position": "Engineer",
      "mobileNumber": "+977 (962) 438-3474",
      "address": "917 Gates Avenue, Townsend, Illinois, 8238"
    },
    {
      "sn": 3,
      "name": "Bernice Mckay",
      "code": 689,
      "joinedDate": "Thu May 21 1970 10:15:08 GMT+0530 (Nepal Time)",
      "gender": "female",
      "email": "bernicemckay@vitricomp.com",
      "position": "Junior Engineer",
      "mobileNumber": "+977 (880) 525-2675",
      "address": "172 Miami Court, Avalon, Guam, 5031"
    },
    {
      "sn": 4,
      "name": "Hendrix Rutledge",
      "code": 596,
      "joinedDate": "Sun Sep 17 1995 02:46:57 GMT+0545 (Nepal Time)",
      "gender": "male",
      "email": "hendrixrutledge@vitricomp.com",
      "position": "Senior Engineer",
      "mobileNumber": "+977 (877) 479-3076",
      "address": "278 Brighton Court, Sterling, Northern Mariana Islands, 8386"
    },
    {
      "sn": 5,
      "name": "Ward Green",
      "code": 653,
      "joinedDate": "Tue Nov 28 2017 09:55:47 GMT+0545 (Nepal Time)",
      "gender": "male",
      "email": "wardgreen@vitricomp.com",
      "position": "Senior Engineer",
      "mobileNumber": "+977 (954) 590-2762",
      "address": "625 Sutton Street, Convent, Connecticut, 771"
    },
    {
      "sn": 6,
      "name": "Shirley Price",
      "code": 789,
      "joinedDate": "Sat Sep 29 2007 04:57:42 GMT+0545 (Nepal Time)",
      "gender": "female",
      "email": "shirleyprice@vitricomp.com",
      "position": "Engineer",
      "mobileNumber": "+977 (800) 575-3610",
      "address": "454 Barbey Street, Dennard, New Mexico, 8713"
    },
    {
      "sn": 7,
      "name": "Marisa Castillo",
      "code": 593,
      "joinedDate": "Mon Jun 24 1985 06:25:48 GMT+0530 (Nepal Time)",
      "gender": "female",
      "email": "marisacastillo@vitricomp.com",
      "position": "Intern Engineer",
      "mobileNumber": "+977 (952) 455-2016",
      "address": "933 Porter Avenue, Wakulla, Iowa, 5458"
    },
    {
      "sn": 8,
      "name": "Robertson Cardenas",
      "code": 196,
      "joinedDate": "Sat Aug 29 1981 17:49:42 GMT+0530 (Nepal Time)",
      "gender": "male",
      "email": "robertsoncardenas@vitricomp.com",
      "position": "Engineer",
      "mobileNumber": "+977 (813) 422-3593",
      "address": "361 Hill Street, Twilight, Marshall Islands, 4314"
    },
    {
      "sn": 9,
      "name": "Elba Franco",
      "code": 775,
      "joinedDate": "Fri Aug 10 2012 03:13:12 GMT+0545 (Nepal Time)",
      "gender": "female",
      "email": "elbafranco@vitricomp.com",
      "position": "Trainee",
      "mobileNumber": "+977 (836) 451-3536",
      "address": "149 Durland Place, Vandiver, Alaska, 2375"
    }
  ]

  constructor() { }

  ngOnInit(): void {

  }

}
