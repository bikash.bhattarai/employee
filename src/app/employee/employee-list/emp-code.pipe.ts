import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'empCode'
})
export class EmpCodePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return `EMP-${value}`;
  }

}
